FROM tiangolo/uwsgi-nginx-flask:python3.7

RUN pip install -U flask-cors==3.0.8 numpy==1.17.4 pandas==0.25.3

COPY . /
