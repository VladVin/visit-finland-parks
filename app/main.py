import json
from pathlib import Path

from flask import Flask, jsonify
from flask import request
from flask_cors import CORS

from model import Model


PARKS_PATH = Path(__file__).resolve().parent.parent / "data" / "parks.json"
PARKS_LOAD_PATH = Path(__file__).resolve().parent.parent / "data" / "all.pkl"

model = Model(PARKS_LOAD_PATH)

app = Flask(__name__)
CORS(app)


@app.route("/api/get_static_info", methods=['GET'])
def get_static_info():
    with open(PARKS_PATH, 'r') as f:
        parks = json.load(f)

    return jsonify(parks)


@app.route("/api/get_full_info", methods=['GET'])
def get_full_info():
    ts_start = int(request.args.get('ts_start'))
    ts_end = int(request.args.get('ts_end'))
    park_id = int(request.args.get('park_id'))

    full_info = model.get_full_info(ts_start, ts_end, park_id)

    return jsonify(full_info)


@app.route("/api/get_recommendations", methods=['GET'])
def get_recommendations():
    ts_start = int(request.args.get('ts_start'))
    ts_end = int(request.args.get('ts_end'))

    recommendations = model.get_recommendations(ts_start, ts_end)

    return jsonify(recommendations)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=80, use_reloader=False)
