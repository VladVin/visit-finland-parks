from operator import itemgetter
import pickle

import pandas as pd
import numpy as np


class Model:
    def __init__(self, parks_load_path):
        self._parks_load = pd.read_pickle(parks_load_path)
        self._max_vals = {
            1: np.max(self._parks_load[self._parks_load['park_id'] == 1]['Predicted']),
            2: np.max(self._parks_load[self._parks_load['park_id'] == 2]['Predicted']),
        }
        self._thresholds = [0.1, 0.5]

    def get_recommendations(self, ts_start, ts_end):
        start_date = pd.datetime.fromtimestamp(ts_start // 10 ** 3).replace(hour=0, minute=0, second=0)
        end_date = pd.datetime.fromtimestamp(ts_end // 10 ** 3).replace(hour=0, minute=0, second=0)

        selected = self._parks_load[self._parks_load['Date'].isin(pd.date_range(start_date, end_date, freq='D'))]

        max_counts = selected.groupby('park_id')['Predicted'].max()
        park_ids, counts = max_counts.index, max_counts.values
        for i, park_id in enumerate(park_ids):
            counts[i] = counts[i] / self._max_vals[park_id]

        park_counts = list(zip(park_ids, counts))
        less_pop = sorted(park_counts, key=itemgetter(1))

        recommendations = {
            'loads': {},
            'recommended': []
        }
        for park_id, pop_num in less_pop:
            if pop_num > self._thresholds[1]:
                pop_index = 2
            elif (pop_num > self._thresholds[0]) and (pop_num <= self._thresholds[1]):
                pop_index = 1
            else:
                pop_index = 0

            recommendations['loads'][park_id] = pop_index
            recommendations['recommended'].append(park_id)

        # add default loads for parks 3-5
        for park_id in [3, 4, 5]:
            recommendations['loads'][park_id] = 2
            recommendations['recommended'].append(park_id)

        return recommendations

    def get_full_info(self, ts_start, ts_end, park_id):
        start_date = pd.datetime.fromtimestamp(ts_start // 10 ** 3).replace(hour=0, minute=0, second=0)
        end_date = pd.datetime.fromtimestamp(ts_end // 10 ** 3).replace(hour=0, minute=0, second=0)

        selected = self._parks_load[self._parks_load['Date'].isin(pd.date_range(start_date, end_date, freq='D'))]
        selected = selected[selected['park_id'] == park_id]

        full_info = {
            'attendance_real': selected['Real'].tolist(),
            'attendance_pred': selected['Predicted'].astype(int).tolist(),
            'temperature': selected['temp'].tolist(),
            'snow': selected['snow'].tolist(),
            'prec': selected['prec'].tolist()
        }

        return full_info
